const boundCopy = (to, from, thisArg) => {
  const props = Object.getOwnPropertyNames(from);

  for (const prop of props) {
    if (thisArg && typeof from[prop] === 'function')
      to[prop] = from[prop].bind(thisArg);
    else to[prop] = from[prop];
  }
};

const defaults = {
  baseUrl: '',
  headers: {
    defaults: {},
    get: {},
    post: {
      'Content-Type': 'application/json',
    },
    put: {
      'Content-Type': 'application/json',
    },
    patch: {
      'Content-Type': 'application/json',
    },
    delete: {},
  },
};

function createInstance() {
  function getHeaders(method) {
    return {
      ...this.defaults.headers.defaults,
      ...this.defaults.headers[method],
    };
  }

  class Requests {
    constructor() {
      this.defaults = { ...defaults };
    }
    get(url) {
      return fetch(`${this.defaults.baseUrl}${url}`, {
        method: 'GET',
        headers: getHeaders.call(this, 'get'),
      });
    }

    post(url, data) {
      return fetch(`${this.defaults.baseUrl}${url}`, {
        method: 'POST',
        headers: getHeaders.call(this, 'post'),
        body: JSON.stringify(data),
      });
    }

    put(url, data) {
      return fetch(`${this.defaults.baseUrl}${url}`, {
        method: 'PUT',
        headers: getHeaders.call(this, 'put'),
        body: JSON.stringify(data),
      });
    }

    patch(url, data) {
      return fetch(`${this.defaults.baseUrl}${url}`, {
        method: 'PATCH',
        headers: getHeaders.call(this, 'patch'),
        body: JSON.stringify(data),
      });
    }

    delete(url) {
      return fetch(`${this.defaults.baseUrl}${url}`, {
        method: 'DELETE',
        headers: getHeaders.call(this, 'delete'),
      });
    }
  }

  const instance = new Requests();
  const shortcut = instance.get.bind(instance);
  boundCopy(shortcut, instance);
  boundCopy(shortcut, Requests.prototype, instance);

  return shortcut;
}

const Requests = createInstance();

export default Requests;
