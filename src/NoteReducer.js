function NoteReducer(state, action) {
  switch (action.type) {
    case 'LOAD_KEEPS':
      const keeps = action.data;
      keeps.sort(
        ({ created_at: a }, { created_at: b }) => Date.parse(b) - Date.parse(a)
      );
      return keeps;
    case 'ADD_KEEP':
      return [action.keep, ...state];
    case 'UPDATE_KEEP':
      const { update } = action;
      return state.map((note) => {
        if (note.id === action.id) return { ...note, ...update };
        else return note;
      });
    case 'DELETE_KEEP':
      return state.filter((note) => note.id !== action.id);
    default:
      return state;
  }
}

export default NoteReducer;
