import React, { useReducer } from 'react';
import { Router } from '@reach/router';
import styled from '@emotion/styled';
import Dashboard from './components/Dashboard/Dashboard';
import Home from './components/Home/Home';
import Trash from './components/Trash/Trash';
import KeepContext from './context/KeepContext';
import NoteReducer from './NoteReducer';
import Requests from './services/requests';
Requests.defaults.baseUrl = 'http://localhost:3000'

const AppContainer = styled.div`
  display: flex;
  flex: 1;
`;

function App() {
  const [notes, dispatch] = useReducer(NoteReducer, []);
  return (
    <Dashboard>
      <KeepContext.Provider value={dispatch}>
        <Router component={AppContainer}>
          <Home notes={notes} path="/" />
          <Trash notes={notes} path="/trash" />
        </Router>
      </KeepContext.Provider>
    </Dashboard>
  );
}

export default App;
