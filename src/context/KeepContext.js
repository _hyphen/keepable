import React from 'react';

const KeepContext = React.createContext(
  (action = { type: '' }) => {}
);

export default KeepContext;
