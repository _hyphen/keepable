import styled from '@emotion/styled';

const Button = styled.button`
  background: none;
  border: none;
  font-family: inherit;
  font-weight: bold;
  padding: 6px;
`;

export default Button