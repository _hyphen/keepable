/** @jsx jsx */
import { useEffect } from 'react';
import ReactDOM from 'react-dom';
import { jsx,css } from '@emotion/core';
import { useState } from 'react';

const KeepModal = ({
  modalStyles,
  modalBg = '#0007',
  onExit = () => { },
  ...props
}) => {
  const [DOMContainer, setDOMContainer] = useState(null);

  useEffect(() => {
    const modalDOM = document.createElement('div');
    document.body.appendChild(modalDOM);
    setDOMContainer(modalDOM);
    return () => modalDOM.remove();
  }, []);

  if (DOMContainer == null)
    return null;

  const Modal = (
    <div
      onClick={(e) => {
        e.stopPropagation();
        if (e.target === e.currentTarget) {
          onExit();
        }
      }}
      css={css`
        position: fixed;
        top: 0;
        bottom: 0;
        right: 0;
        left: 0;
        background: ${modalBg};
        animation: fadeIn 0.15s ease-in-out forwards;
        ${modalStyles};
        @keyframes fadeIn {
          from {
            background: #0000;
          }
          to {
            background: ${modalBg};
          }
        }
      `}
      {...props} />
  );

  return ReactDOM.createPortal(Modal, DOMContainer);
};

export default KeepModal