import styled from '@emotion/styled';

const IconBtn = styled.button`
  border: none;
  border-radius: 50%;
  padding: 0;
  width: 32px;
  height: 32px;
  background: rgba(255, 255, 255, 0.5);
  cursor: pointer;
  transition: background .1s ease-in ;
  &:hover{
    background: #0008;
    img{
      filter: brightness(1.6);
    }
  }
`;

export default IconBtn;
