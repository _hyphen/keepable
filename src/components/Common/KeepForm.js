/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import styled from '@emotion/styled';
import { useState, useContext, useEffect, useRef } from 'react';
import Button from '../Common/Button';
import Palette, { COLORS } from '../Common/Palette';
import KeepContext from '../../context/KeepContext';
import IconBtn from './IconButton';
import { useSpring, a } from 'react-spring';
import Requests from '../../services/requests';

const StyledForm = styled.form`
  background: #ffffff;
  box-shadow: 5px 5px 15px rgba(153, 155, 158, 0.85);
  border-radius: 8px;
  padding: 20px 16px;
  width: 50%;
  max-width: 600px;
  transition: background 0.1s ease-out;
`;

const Input = styled.input`
  width: 100%;
  border: none;
  background: none;
`;

const Controls = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: space-between;
`;

function getDefaultColor(defaults) {
  if (defaults) return { color: defaults.color, value: COLORS[defaults.color] };
  else return { color: 'white', value: '#fff' };
}

function useMeasure() {
  const ref = useRef();
  const [bounds, set] = useState({ left: 0, top: 0, width: 0, height: 0 });

  useEffect(() => {
    if (ref.current) {
      set(ref.current.getBoundingClientRect());
    }
  }, []);
  return [{ ref }, bounds];
}

const Expanded = ({ expand, children }) => {
  const [bind, { height: viewHeight }] = useMeasure();
  const { height, opacity } = useSpring({
    from: { height: 0, opacity: 0 },
    to: {
      height: expand ? viewHeight : 0,
      opacity: expand ? 1 : 0,
    },
  });
  return (
    <a.div style={{ height: height, opacity }}>
      <a.div {...bind}>{children}</a.div>
    </a.div>
  );
};

const TitleInput = styled(Input)`
  margin-bottom: 10px;
  &::placeholder {
    font-weight: bold;
    color: #000;
    opacity: 1;
  }
`;
const BodyInput = styled(Input)`
  font-size: 18px;
  line-height: 24px;
  letter-spacing: 0.25px;
`;

const FormError = styled.span`
  color: #000;
  align-self: center;
  font-size: 12.5px;
  font-weight: bold;
`;

const PinnedBtn = styled(IconBtn)`
  position: absolute;
  right: 0;
  top: 0;
  img {
    vertical-align: middle;
    ${({ pinned }) => (pinned ? { filter: 'brightness(0.6)' } : null)};
  }
`;

const SubmitKeepButton = styled(Button)`
  font-size: 18px;
  line-height: 24px;
  letter-spacing: 0.25px;
  cursor: pointer;
  border-radius: 4px;
  border: 1px solid transparent;
  transition: border-color .2s ease;
  &:hover {
    border-color: #282828;
  }
`;

const KeepForm = ({
  defaults = {},
  onSubmit = () => {},
  isCompact = false,
  formStyles,
  ...props
}) => {
  const [color, setColor] = useState(getDefaultColor(defaults));
  const [pinned, setPinned] = useState(!!defaults.pinned);
  const [compact, setCompact] = useState(isCompact);
  const [errors, setErrors] = useState({});
  const [hoverColor, setHoverColor] = useState(null);
  const dispatch = useContext(KeepContext);

  useEffect(() => {
    const shrink = (e) => {
      setCompact(true);
    };
    if (isCompact) {
      window.addEventListener('click', shrink);
      return () => window.removeEventListener('click', shrink);
    }
  }, [isCompact]);

  const handleSubmit = (e) => {
    e.preventDefault();
    const form = e.target;

    const errors = {};

    const data = {
      title: form.elements['title'].value,
      body: form.elements['body'].value,
      color: color.color,
      pinned,
    };

    if (data.title.length < 3)
      errors.title = 'Title must be at least 3 character long';

    if (Object.values(errors).some((value) => !!value))
      return setErrors(errors);

    if (defaults.id) {
      /* update */
      Requests.patch(`/notes/${defaults.id}`, data)
        .then((r) => r.json())
        .then((update) =>
          dispatch({ type: 'UPDATE_KEEP', update, id: defaults.id })
        );
    } else {
      /* add another */
      Requests.post(`/notes`, data)
        .then((r) => r.json())
        .then((keep) => dispatch({ type: 'ADD_KEEP', keep }));
    }

    form.reset();

    onSubmit();
  };

  const handleDelete = (e) => {
    e.stopPropagation();
    const { deleted_at, id } = defaults;
    Requests.delete(`/notes/${id}`).then(() => {
      if (deleted_at) dispatch({ type: 'DELETE_KEEP', id });
      else {
        dispatch({
          type: 'UPDATE_KEEP',
          update: { deleted_at: Date.now() },
          id,
        });
      }
    });
  };

  const validateTitle = ({ target }) => {
    if (errors.title && target.value.length >= 3) setErrors({ title: null });
  };

  return (
    <StyledForm
      css={css`
        background: ${hoverColor || color.value};
        position: relative;
        ${formStyles};
      `}
      onClick={(e) => e.stopPropagation()}
      onFocus={() => {
        if (isCompact) {
          setCompact(false);
        }
      }}
      onSubmit={handleSubmit}
    >
      <Expanded expand={!compact}>
        <PinnedBtn
          pinned={pinned}
          type="button"
          onClick={() => setPinned((p) => !p)}
        >
          <img alt="Pin Keep" src={require('../../images/icons/pin.svg')} />
        </PinnedBtn>
        <TitleInput
          name="title"
          onChange={validateTitle}
          defaultValue={defaults.title}
          type="text"
          placeholder="The title for my new note"
        />
      </Expanded>
      <BodyInput
        name="body"
        defaultValue={defaults.body}
        type="text"
        placeholder="Some great thought!"
      />
      <Expanded expand={!compact}>
        <Controls>
          <div
            css={css`
              display: flex;
            `}
          >
            <Palette onChangeColor={setColor} onHover={setHoverColor} />
            {defaults.id && (
              <IconBtn
                type="button"
                onClick={handleDelete}
                style={{ marginLeft: 5 }}
              >
                <img
                  alt="Delete"
                  src={require('../../images/icons/trash.svg')}
                />
              </IconBtn>
            )}
          </div>
          {errors.title && <FormError>{errors.title}</FormError>}
          <SubmitKeepButton type="submit">Keep it</SubmitKeepButton>
        </Controls>
      </Expanded>
    </StyledForm>
  );
};

export default KeepForm;
