/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import { useState, useEffect, useRef } from 'react';
import styled from '@emotion/styled';
import IconBtn from './IconButton';

export const COLORS = {
  white: '#fff',
  salmon: '#F28B82',
  orange: '#FBBC04',
  yellow: '#FFF475',
  green: '#CCFF90',
  teal: '#A7FFEB',
  light_blue: '#CBF0F8',
  blue: '#AECBFA',
  purple: '#D7AEFB',
  pink: '#FDCFE8',
};

const PaletteBtn = styled.button`
  border: none;
  width: 24px;
  height: 24px;
  border-radius: 50%;
  margin: 2.5px 0;
`;

const PaletteContainer = styled.div`
  position: absolute;
  z-index: 10;
  bottom: 100%;
  left: 0;
  padding: 2.5px 5px;
  width: 140px;
  background: #fff;
  display: flex;
  flex-wrap: wrap;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 8px;
  border-radius: 5px;
  justify-content: space-between;
  animation: popIn 0.13s ease-in forwards;
  @keyframes popIn {
    from {
      opacity: 0.25;
    }
    to {
      opacity: 1;
    }
  }
`;

const paletteActive = css`
  background: gray;
`;

const Palette = ({ onChangeColor, onHover }) => {
  const [showPalette, setShowPalette] = useState(false);
  const paletteRef = useRef(null);

  useEffect(() => {
    const hidePalette = (e) => {
      if (e.target !== paletteRef.current) {
        setShowPalette(false);
      }
    };

    if (showPalette && paletteRef) {
      document.addEventListener('click', hidePalette);
    }

    return () => document.removeEventListener('click', hidePalette);
  }, [showPalette]);

  return (
    <div
      css={css`
        position: relative;
      `}
    >
      <IconBtn
        css={showPalette ? paletteActive : null}
        type="button"
        onClick={(event) => {
          event.stopPropagation();
          setShowPalette(true);
        }}
      >
        <img
          css={
            showPalette
              ? css`
                  filter: brightness(160%);
                `
              : null
          }
          alt="Change color"
          style={{ verticalAlign: 'middle' }}
          src={require('../../images/icons/color-picker.svg')}
        />
      </IconBtn>
      {showPalette && (
        <PaletteContainer
          onClick={(event) => event.stopPropagation()}
          ref={paletteRef}
        >
          {Object.keys(COLORS).map((colorName) => (
            <PaletteBtn
              key={colorName}
              type="button"
              onMouseEnter={() => onHover(COLORS[colorName])}
              onMouseLeave={() => onHover(null)}
              onClick={(event) => {
                event.nativeEvent.stopImmediatePropagation();
                onChangeColor({ color: colorName, value: COLORS[colorName] });
                setShowPalette(false);
              }}
              style={colorName === 'white' ? { border: '1px solid gray' } : {}}
              css={css`
                background: ${COLORS[colorName]};
              `}
            />
          ))}
        </PaletteContainer>
      )}
    </div>
  );
};

export default Palette;
