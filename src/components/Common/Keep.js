/** @jsx jsx */
/** @jsxFrag React.Fragment */
/* eslint-disable-next-line */
import React, { useContext, useRef } from 'react';
import { css, jsx } from '@emotion/core';
import styled from '@emotion/styled';
import Palette, { COLORS } from './Palette';
import { useState } from 'react';
import IconBtn from './IconButton';
import KeepContext from '../../context/KeepContext';
import KeepForm from './KeepForm';
import KeepModal from './KeepModal';
import Requests from '../../services/requests';

const KeepContainer = styled.div`
  padding: 20px 20px 5px;
  box-shadow: 5px 5px 15px rgba(153, 155, 158, 0.85);
  border-radius: 8px;
  display: flex;
  flex-direction: column;
  color: #000000;
  min-height: 200px;
  max-width: 200px;
  transition: background 0.1s ease-out;
  animation:  sizeKeepIn 0.42s cubic-bezier(0.46, 0.03, 0.52, 0.96) forwards;
  @keyframes sizeKeepIn {
    from {
      transform: scale3d(0.34, 0.34, 0.34);
      opacity:0;
    }
    30%{
      opacity:0;
    }
    to {
      opacity: 1;
      transform: scale3d(1, 1, 1);
    }
  }
`;

const KeepTitle = styled.h4`
  font-weight: bold;
  font-size: 14px;
  line-height: 20px;
  letter-spacing: 0.25px;
  margin: 0 0 10px;
`;

const KeepBody = styled.p`
  font-size: 18px;
  line-height: 24px;
  flex: 1;
  letter-spacing: 0.25px;
`;

const KeepControls = styled.div`
  display: flex;
`;

const Keep = (keepProps) => {
  const { title, body, pinned, deleted_at, id, ...rest } = keepProps;
  const [hoverColor, setHoverColor] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const dispatch = useContext(KeepContext);

  const handleUpdate = (update) => {
    Requests.patch(`/notes/${id}`, update)
      .then((r) => r.json())
      .then((update) => dispatch({ type: 'UPDATE_KEEP', update, id }));
  };

  const handleDelete = (e) => {
    e.stopPropagation();
    Requests.delete(`/notes/${id}`).then(() => {
      if (deleted_at) dispatch({ type: 'DELETE_KEEP', id });
      else {
        dispatch({
          type: 'UPDATE_KEEP',
          update: { deleted_at: Date.now() },
          id,
        });
      }
    });
  };

  const openEditModal = (e) => {
    if (!deleted_at) setShowModal(true);
  };
  const handleRecover = (event) => {
    event.stopPropagation();
    handleUpdate({ deleted_at: null });
  };
  const toggleNotePin = (event) => {
    event.stopPropagation();
    if (!deleted_at) handleUpdate({ pinned: !pinned });
  };

  return (
    <KeepContainer
      onClick={openEditModal}
      css={css`
        background: ${hoverColor || COLORS[rest.color]};
        position: relative;
      `}
    >
      {showModal && (
        <KeepModal onExit={() => setShowModal(false)}>
          <KeepForm
            defaults={keepProps}
            onSubmit={() => setShowModal(false)}
            formStyles={css`
              position: absolute;
              top: 20%;
              left: 50%;
              transform: translate3d(-30%, 0, 0);
            `}
          />
        </KeepModal>
      )}
      <IconBtn
        onClick={toggleNotePin}
        css={css`
          position: absolute;
          right: 0;
          top: 0;
          img {
            vertical-align: middle;
            ${pinned ? { filter: 'brightness(0.6)' } : null};
          }
        `}
      >
        <img alt="Pin Keep" src={require('../../images/icons/pin.svg')} />
      </IconBtn>
      <KeepTitle>{title}</KeepTitle>
      <KeepBody>{body}</KeepBody>
      <KeepControls>
        {deleted_at === null ? (
          <>
            <Palette
              onChangeColor={({ color }) => handleUpdate({ color })}
              onHover={setHoverColor}
            />
            <IconBtn
              type="button"
              css={css`
                margin-left: 5px;
              `}
              onClick={handleDelete}
            >
              <img alt="Delete" src={require('../../images/icons/trash.svg')} />
            </IconBtn>
          </>
        ) : (
          <>
            <IconBtn type="button" onClick={handleDelete}>
              <img alt="Delete" src={require('../../images/icons/trash.svg')} />
            </IconBtn>
            <IconBtn
              type="button"
              onClick={handleRecover}
              css={css`
                margin-left: 5px;
              `}
            >
              <img
                alt="Recover"
                src={require('../../images/icons/restore.svg')}
              />
            </IconBtn>
          </>
        )}
      </KeepControls>
    </KeepContainer>
  );
};

export default Keep;
