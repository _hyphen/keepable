/** @jsxFrag React.Fragment */
/** @jsx jsx */
/* eslint-disable-next-line */
import React from 'react';
import { jsx, css } from '@emotion/core';
import styled from '@emotion/styled';
import { useEffect, useContext } from 'react';
import Keep from '../Common/Keep';
import KeepContext from '../../context/KeepContext';
import KeepForm from '../Common/KeepForm';
import Requests from '../../services/requests';

const KeepGrid = styled.div`
  display: grid;
  grid: auto-flow / repeat(auto-fill, 240px);
  gap: 20px;
  justify-content: center;
`;

const SectionTitle = styled.h2`
  font-weight: bold;
  font-size: 14px;
  line-height: 24px;
  letter-spacing: 0.25px;
  color: #ffffff;
  margin-bottom: 20px;
  text-transform: uppercase;
  grid-column: 1/-1;
  animation: slideIn 0.25s ease-in forwards;
  @keyframes slideIn {
    from {
      opacity: 0;
      transform: translate3d(-5%, 0, 0);
    }
    60% {
      opacity: 0.5;
    }
  }
`;

const KeepSection = ({ title, containerStyles, children, ...props }) => {
  return (
    <section
      css={css`
        margin: 60px auto;
        width: 100%;
        max-width: 1060px;
        padding: 0 20px;
        box-sizing: border-box;
        ${containerStyles};
      `}
    >
      <KeepGrid {...props}>
        <SectionTitle>{title}</SectionTitle>
        {children}
      </KeepGrid>
    </section>
  );
};

const Container = styled.div`
  margin: 1rem 0;
  flex: 1;
  display: flex;
  flex-direction: column;
`;

const EmptyMessage = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  font-weight: bold;
  align-items: center;
  font-size: 36px;
  line-height: 48px;
  text-align: center;
  letter-spacing: 0.25px;
  max-width: 360px;
  margin: 0 auto;
  &::before,
  &::after {
    position: relative;
    bottom: 7.5px;
    font-size: 144px;
  }
  &::before {
    content: '{';
  }
  &::after {
    content: '}';
  }
`;

const Home = ({ notes }) => {
  const dispatch = useContext(KeepContext);

  useEffect(() => {
    Requests.get('/notes')
      .then((r) => r.json())
      .then((data) => dispatch({ type: 'LOAD_KEEPS', data }));
  }, [dispatch]);
  const hasPinned = notes.some((note) => note.pinned);
  const availableNotes = notes.filter((note) => !note.deleted_at);

  return (
    <Container>
      <KeepForm formStyles={{ margin: '0 auto' }} isCompact />
      {availableNotes.length ? (
        <>
          {hasPinned && (
            <KeepSection containerStyles={{ marginBottom: 0 }} title="Pinned">
              {availableNotes
                .filter((note) => note.pinned)
                .map((note) => (
                  <Keep key={note.id} {...note} />
                ))}
            </KeepSection>
          )}
          <KeepSection title="Others">
            {availableNotes
              .filter((note) => !note.pinned)
              .map((note) => (
                <Keep key={note.id} {...note} />
              ))}
          </KeepSection>
        </>
      ) : (
        <EmptyMessage>Notes you add appear here</EmptyMessage>
      )}
    </Container>
  );
};
export default Home;
