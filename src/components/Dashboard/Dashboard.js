/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import styled from '@emotion/styled';
import { Link } from '@reach/router';

const isActive = ({ isCurrent }) =>
  isCurrent
    ? {
        style: { background: '#999B9E' },
      }
    : {};

const NavLink = (props) => (
  <Link
    css={css`
      padding: 0.5rem 0 0.5rem 1rem;
      margin-bottom: 1rem;
      color: #fff;
      display: block;
      border-radius: 0 25px 25px 0;
      text-decoration: none;
    `}
    getProps={isActive}
    {...props}
  />
);

const AppHeader = styled.header`
  padding: 1rem 0;
  display: grid;
  grid: 1fr / 20rem 1fr;
  justify-content: center;
  align-items: center;
  font-weight: bold;
  border-bottom: 2px solid #d1d1d1;
`;

const Icon = styled.img`
  margin-right: 0.5rem;
  vertical-align: bottom;
`;

const SideNav = styled.nav`
  max-width: 280px;
  flex: 1;
  min-height: 100%;
  border-right: 2px solid #d1d1d1;
`;
const LinkList = styled.ul`
  margin: 0.5rem 0 0;
  list-style: none;
`;
const DashboardContainer = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;
const Dashboard = ({ children }) => {
  return (
    <DashboardContainer>
      <AppHeader>
        <img
          css={css`
            margin-left: 1rem;
          `}
          height="24"
          alt="Keepable"
          src={require('../../images/logo.png')}
        />
        <p>{'Welcome to {keepable}'}</p>
      </AppHeader>
      <main
        css={css`
          display: flex;
          flex: 1;
        `}
      >
        <SideNav>
          <LinkList>
            <li>
              <NavLink to="/">
                <Icon
                  alt="Notes"
                  src={require('../../images/icons/code.svg')}
                />
                Notes
              </NavLink>
            </li>
            <li>
              <NavLink to="/trash">
                <Icon
                  alt="Trash"
                  css={css`filter: brightness(1.6)`}
                  src={require('../../images/icons/trash.svg')}
                />
                Trash
              </NavLink>
            </li>
          </LinkList>
        </SideNav>
        {children}
      </main>
    </DashboardContainer>
  );
};

export default Dashboard;
