import React from 'react';
import styled from '@emotion/styled';
import { useContext, useEffect } from 'react';
import KeepContext from '../../context/KeepContext';
import Keep from '../Common/Keep';
import Requests from '../../services/requests';

const KeepGrid = styled.div`
  display: grid;
  grid: auto-flow / repeat(auto-fill, 240px);
  gap: 20px;
  justify-content: center;
`;

const Section = styled.div`
  max-width: 1060px;
  margin: 30px auto;
  padding: 0 20px;
  box-sizing: border-box;
`;

const Container = styled.div`
  margin: 1rem 0;
  flex: 1;
`;

const Trash = ({ notes }) => {
  const dispatch = useContext(KeepContext);

  useEffect(() => {
    Requests('/notes')
      .then((r) => r.json())
      .then((data) => dispatch({ type: 'LOAD_KEEPS', data }));
  }, [dispatch]);

  return (
    <Container>
      <Section>
        <KeepGrid>
          {notes
            .filter((note) => !!note.deleted_at)
            .map((note) => (
              <Keep key={note.id} {...note} />
            ))}
        </KeepGrid>
      </Section>
    </Container>
  );
};

export default Trash;
